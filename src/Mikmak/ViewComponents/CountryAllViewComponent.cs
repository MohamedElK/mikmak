﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mikmak.Models;
using System.Collections;

namespace Mikmak.ViewComponents
{
    public class CountryAllViewComponent : ViewComponent
    {
        private readonly MikmakDbContext _context;

        public CountryAllViewComponent(MikmakDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            IEnumerable list = await _context.Country.ToListAsync();
            return View(list);
        }
    }
}
