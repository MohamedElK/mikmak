﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mikmak.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mikmak.ViewComponents
{
    public class SupplierDropDownViewComponent : ViewComponent
    {
        private readonly MikmakDbContext _context;

        public SupplierDropDownViewComponent(MikmakDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? supplierId)
        {
            IEnumerable list = await _context.Supplier.ToListAsync();
            ViewBag.SupplierId = supplierId;
            return View(list);

            //return View{ IdSupplier, list});

        }

    }

}
