﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mikmak.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mikmak.ViewComponents
{
    public class UnitBaseDropDownViewComponent : ViewComponent
    {
        private readonly MikmakDbContext _context;

        public UnitBaseDropDownViewComponent(MikmakDbContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(int? unitBaseId)
        {
            IEnumerable list = await _context.UnitBase.ToListAsync();
            ViewBag.UnitBaseId = unitBaseId;
            return View(list);

        }

    }
}
