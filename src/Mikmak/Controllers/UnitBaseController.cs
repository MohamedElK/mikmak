using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Mikmak.Models;

namespace Mikmak
{
    public class UnitBaseController : Controller
    {
        private readonly MikmakDbContext _context;

        public UnitBaseController(MikmakDbContext context)
        {
            _context = context;    
        }

        // GET: UnitBase
        public async Task<IActionResult> Index()
        {
            return View(await _context.UnitBase.ToListAsync());
        }

        // GET: UnitBase/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var unitBase = await _context.UnitBase.SingleOrDefaultAsync(m => m.Id == id);
            if (unitBase == null)
            {
                return NotFound();
            }
            unitBase.List = _context.UnitBase.ToList();
            return View(unitBase);
        }

        // GET: UnitBase/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: UnitBase/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Code,Description,Name,ShippingCostMultiplier")] UnitBase unitBase)
        {
            if (ModelState.IsValid)
            {
                _context.Add(unitBase);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(unitBase);
        }

        // GET: UnitBase/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var unitBase = await _context.UnitBase.SingleOrDefaultAsync(m => m.Id == id);
            if (unitBase == null)
            {
                return NotFound();
            }
            return View(unitBase);
        }

        // POST: UnitBase/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Code,Description,Name,ShippingCostMultiplier")] UnitBase unitBase)
        {
            if (id != unitBase.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(unitBase);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UnitBaseExists(unitBase.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(unitBase);
        }

        // GET: UnitBase/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var unitBase = await _context.UnitBase.SingleOrDefaultAsync(m => m.Id == id);
            if (unitBase == null)
            {
                return NotFound();
            }

            return View(unitBase);
        }

        // POST: UnitBase/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var unitBase = await _context.UnitBase.SingleOrDefaultAsync(m => m.Id == id);
            _context.UnitBase.Remove(unitBase);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool UnitBaseExists(int id)
        {
            return _context.UnitBase.Any(e => e.Id == id);
        }
    }
}
