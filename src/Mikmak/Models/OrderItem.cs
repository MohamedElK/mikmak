﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class OrderItem
    {
        public int ProductId { get; set; }
        public int OrderId { get; set; }
        [Key]
        public int Id { get; set; }
        public decimal? Quantity { get; set; }

        [NotMapped]
        // De Id postfix wordt automatisch toegevoegd
        public IEnumerable<Product> ProductList { get; set; }

        [NotMapped]
        public virtual Product Product { get; set; }

        [NotMapped]
        // De Id postfix wordt automatisch toegevoegd
        public IEnumerable<Order> OrderList { get; set; }

        [NotMapped]
        public virtual Order Order { get; set; }


        //[NotMapped]
        //public virtual Order IdOrderNavigation { get; set; }
        //[NotMapped]
        //public virtual Product IdProductNavigation { get; set; }
    }
}
