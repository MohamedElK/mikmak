﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class Customer
    {
        public Customer()
        {
            //Order = new HashSet<Order>();
        }

        [Required(ErrorMessage = "Roepnaam is verplicht in te vullen")]
        [DisplayName("Roepnaam")]
        public string NickName { get; set; }

        [Required(ErrorMessage = "Voornaam is verplicht in te vullen")]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Familienaam is verplicht in te vullen")]
        [DisplayName("Familienaam")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Adres is verplicht in te vullen")]
        [DisplayName("Adres 1")]
        public string Address1 { get; set; }

        [DisplayName("Adres 2")]
        public string Address2 { get; set; }

        [DisplayName("Stad")]
        [Required(ErrorMessage = "Stad is verplicht in te vullen")]
        public string City { get; set; }

        [DisplayName("Regio")]
        public string Region { get; set; }

        [Required(ErrorMessage = "Postcode is verplicht in te vullen")]
        [DisplayName("PostCpde")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Gelieve een land te kiezen")]
        [DisplayName("Land")]
        public int CountryId { get; set; }

        [DisplayName("Telefoon")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Mobiel is verplicht in te vullen")]
        [DisplayName("Mobieltje")]
        public string Mobile { get; set; }

        [Key]
        public int Id { get; set; }

        [NotMapped]
        // De Id postfix wordt automatisch toegevoegd
        public IEnumerable<Country> CountryList { get; set; }

        [NotMapped]
        public virtual Country Country { get; set; }

        [NotMapped]
        public virtual ICollection<Order> Order { get; set; }
        
        //public virtual Country IdCountryNavigation { get; set; }
    }
}
