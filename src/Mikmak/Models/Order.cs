﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderItem = new HashSet<OrderItem>();
        }

        public DateTime OrderDate { get; set; }
        public DateTime ShippingDate { get; set; }
        public string Comment { get; set; }
        [Key]
        public int Id { get; set; }

        //[ForeignKey("CustomerId")]
        public int CustomerId { get; set; }

        //[ForeignKey("ShippingMethodId")]
        public int ShippingMethodId { get; set; }

        //[ForeignKey("StatusId")]
        public int StatusId { get; set; }

        //[NotMapped]
        // De Id postfix wordt automatisch toegevoegd
        //public IEnumerable<Customer> CustomerList { get; set; }
        //[NotMapped]
        //public virtual Customer Customer { get; set; }

        //[NotMapped]
        // De Id postfix wordt automatisch toegevoegd
        //public IEnumerable<OrderStatus> OrderStatusList { get; set; }
        //[NotMapped]
        //public virtual OrderStatus OrderStatus { get; set; }

        [NotMapped]
        public virtual ICollection<OrderItem> OrderItem { get; set; }

        //public virtual Customer IdCustomerNavigation { get; set; }
        //public virtual ShippingMethod IdShippingMethodNavigation { get; set; }
        //public virtual OrderStatus IdStatusNavigation { get; set; }
    }
}
