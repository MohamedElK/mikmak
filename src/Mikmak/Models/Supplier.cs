﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class Supplier
    {
        public Supplier()
        {
        }

        [Required(ErrorMessage = "Het veld Code moet zijn ingevuld!")]
        [MaxLength(10, ErrorMessage = "Leverancierscode bestaat uit maximum 10 tekens.")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Het veld Leveranciersnaam moet zijn ingevuld!")]
        [MaxLength(256, ErrorMessage = "Leveranciersnaam bestaat uit maximum 256 tekens.")]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [MaxLength(256, ErrorMessage = "Leverancierscontact bestaat uit maximum 256 tekens.")]
        public string Contact { get; set; }

        [DisplayName("Adres")]
        [MaxLength(256, ErrorMessage = "Leveranciersadres bestaat uit maximum 256 tekens.")]
        public string Address { get; set; }

        [DisplayName("Gemeente")]
        [MaxLength(256, ErrorMessage = "Stad bestaat uit maximum 256 tekens.")]
        public string City { get; set; }

        [DisplayName("Regio")]
        [MaxLength(80, ErrorMessage = "Regio bestaat uit maximum 80 tekens.")]
        public string Region { get; set; }

        [DisplayName("Postcode")]
        [MaxLength(20, ErrorMessage = "Postcode bestaat uit maximum 20 tekens.")]
        public string PostalCode { get; set; }

        [DisplayName("Telefoon")]
        [MaxLength(40, ErrorMessage = "Telefoon bestaat uit maximum 40 tekens.")]
        public string Phone { get; set; }

        [DisplayName("Mobiel")]
        [MaxLength(40, ErrorMessage = "Mobiel bestaat uit maximum 40 tekens.")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "Gelieve een land te selecteren!")]
        public int CountryId { get; set; }

        [Key]
        public int Id { get; set; }

        [NotMapped]
        // De Id postfix wordt automatisch toegevoegd
        public IEnumerable<Country> CountryList { get; set; }
        [NotMapped]
        public virtual Country Country { get; set; }
    }
}
