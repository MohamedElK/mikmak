﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class ShippingMethod
    {
        public ShippingMethod()
        {
            //Order = new HashSet<Order>();
        }

        [Required(ErrorMessage = "Naam is verplicht in te vullen")]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "Prijs is verplicht in te vullen")]
        public decimal Price { get; set; }

        [Key]
        public int Id { get; set; }

        [NotMapped]
        public virtual ICollection<Order> Order { get; set; }

        [NotMapped]
        public virtual ICollection<ShippingMethod> List { get; set; }
    }
}
