﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class Country
    {
        public Country()
        {
            //Customer = new HashSet<Customer>();
            //Supplier = new HashSet<Supplier>();
        }

        [Required(ErrorMessage = "Het veld Code moet zijn ingevuld!")]
        public string Code { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        [Required(ErrorMessage = "Het veld Verzendingkostfactor moet zijn ingevuld!")]
        public string Name { get; set; }
        public double? ShippingCostMultiplier { get; set; }

        [Required]
        [Key]
        public int Id { get; set; }

        [NotMapped]
        public virtual ICollection<Country> List { get; set; }
     }
}
