﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class Product
    {
        public Product()
        {
            //OrderItem = new HashSet<OrderItem>();
        }

        [DisplayName("Beschrijving")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Naam is verplicht in te vullen")]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [DisplayName("Prijs")]
        [Range(0.01, 1000.00, ErrorMessage = "Prijs moet tussen 0 en 1000 liggen")]
        public double? Price { get; set; }

        [DisplayName("Verzendkosten")]
        public double? ShippingCost { get; set; }

        [DisplayName("Totaal Rating")]
        public int? TotalRating { get; set; }

        [DisplayName("Miniatuur")]
        public string Thumbnail { get; set; }

        [DisplayName("Afbeelding")]
        public string Image { get; set; }

        [DisplayName("Aanbiedingspercent")]
        public double? DiscountPercentage { get; set; }

        [DisplayName("Stemmen")]
        public int? Votes { get; set; }

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Gelieve een basiseenheid te kiezen")]
        [DisplayName("Basiseenheid")]
        public int UnitBaseId { get; set; }

        [Required(ErrorMessage = "Gelieve een leverancier te kiezen")]
        [DisplayName("Leverancier")]
        public int SupplierId { get; set; }

        [NotMapped]
        // De Id postfix wordt automatisch toegevoegd
        public IEnumerable<Supplier> SupplierList { get; set; }

        [NotMapped]
        public IEnumerable<UnitBase> UnitBaseList { get; set; }
        
        [NotMapped]
        public virtual Supplier Supplier { get; set; }

        [NotMapped]
        public virtual UnitBase UnitBase { get; set; }

        [NotMapped]
        public virtual ICollection<OrderItem> OrderItem { get; set; }
        
        //public virtual Supplier IdSupplierNavigation { get; set; }
    }
}
