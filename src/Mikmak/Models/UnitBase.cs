﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class UnitBase
    {

        [Required(ErrorMessage = "{0} is verplicht.")]
        [MaxLength(255, ErrorMessage = "{0} kan maximum 255 karakters bevatten.")]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [MaxLength(1024, ErrorMessage = "{0} kan maximum 1024 karakters bevatten.")]
        [DisplayName("Beschrijving")]
        public string Description { get; set; }

        [DisplayName("Verzendingskostfactor")]
        public double? ShippingCostMultiplier { get; set; }

        [Required(ErrorMessage = "{0} is verplicht.")]
        [MaxLength(2, ErrorMessage = "{0} kan maximum 2 karakters bevatten.")]
        [DisplayName("Code")]
        public string Code { get; set; }

        [Key]
        public int Id { get; set; }

        [NotMapped]
        public virtual ICollection<UnitBase> List { get; set; }
    }
}
