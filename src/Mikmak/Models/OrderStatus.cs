﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikmak.Models
{
    public partial class OrderStatus
    {
        public OrderStatus()
        {
            Order = new HashSet<Order>();
        }

        [Required(ErrorMessage = "Naam is verplicht in te vullen")]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Beschrijving is verplicht in te vullen")]
        [DisplayName("Beschrijving")]
        public string Description { get; set; }

        [Key]
        public int Id { get; set; }

        [NotMapped]
        public virtual ICollection<Order> Order { get; set; }
    }
}
